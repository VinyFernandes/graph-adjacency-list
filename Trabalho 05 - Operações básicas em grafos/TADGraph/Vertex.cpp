/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Vertex.cpp
 * Author: vinicius
 * 
 * Created on 6 de Julho de 2018, 20:01
 */

#include <stddef.h>

#include "Vertex.h"

/**
 * 
 */
Vertex::Vertex() {
    this->key = 0;
    this->label = this->key;
}

/**
 * 
 * @param key
 * @param label
 */
Vertex::Vertex(int key, string label) {
    this->key = key;
    this->label = label;
}

/**
 * 
 */
Vertex::~Vertex() {
}

/**
 * 
 * @param label
 */
void Vertex::SetLabel(string label) {
    this->label = label;
}

/**
 * 
 * @return 
 */
string Vertex::GetLabel() const {
    return label;
}

/**
 * 
 * @param key
 */
void Vertex::SetKey(int key) {
    this->key = key;
}

/**
 * 
 * @return 
 */
int Vertex::GetKey() const {
    return key;
}

