/* 
 * File:   GraphAL.h
 * Nome: Representação Por Lista de Adjacencia de um gráfico
 * Descrição: programa que mostra um gráfico em sua forma vinculada
 * Author: vinicius
 *
 * Created on 6 de Julho de 2018, 20:00
 */

#ifndef GRAPHAL_H
#define GRAPHAL_H

#include <iostream>
#include <algorithm>
#include <list>

#include "Vertex.h"
#include "Edge.h"

using namespace std;

class GraphAL {
private:

    int nVertices; // number of vertices of the graph
    int nEdges; // number of edges of the graph
    list<Edge*> *Adj; // pointer to an array containing the adjacent edges
    Vertex **vertices; // array of vertices

public:

    /**
     * class constructor, dynamically creates the vector that points to the 
     * lists of adjacencies of the n vertices of the graph;
     * @param n
     */
    GraphAL(int n);

    /**
     * returns the number of vertices of the graph;
     * @return 
     */
    int GetNVertices();

    /**
     * inserts the edge (u, v) into the graph structure if it does not yet 
     * exists. The edge can be directed or not directed, this is defined by the
     * parameter dir. The parameter c informs the cost of the edge (u, v). 
     * Consider that if the edge is not valued, its cost is zero;
     * @param u
     * @param v
     * @return 
     */
    void InsertEdge(int u, int v, bool dir, int c);

    /**
     * check if there is an edge (u, v), returns true if there is an edge in
     * the graph and false otherwise
     * @param u
     * @param v
     * @return 
     */
    Edge* HasEdge(int u, int v);

    /**
     * check if the edge (u, v) is a directed edge, returns true if positive,
     * false otherwise;
     * @param u
     * @param v
     * @return 
     */
    bool IsDirected(int u, int v);

    /**
     * verifies that the edge (u, v) is evaluated, returns true in case 
     * positive, false otherwise
     * @param u
     * @param v
     * @return 
     */
    bool IsEdgeValued(int u, int v);

    /**
     * returns the value of the edge (u, v) if the edge exists
     * @param u
     * @param v
     * @return 
     */
    int GetEdgeValue(int u, int v);

    /**
     * returns the number of edges of the graph
     * @return 
     */
    int GetNEdges();

    /**
     * removes the edge (u, v) of the graph structure
     * @param u
     * @param v
     * @return 
     */
    Edge* RemoveEdge(int u, int v);

    /**
     * returns all vertices adjacent to vertex v
     * @param v
     * @return 
     */
    list<Vertex*> GetAdjacencyVertex(int v);

    /**
     * returns all edges that have vertex v
     * @param v
     * @return 
     */
    list<Edge*> GetIncidentEdges(int v);

    /**
     * defines a label (string) for vertex v. By default, the vertex label 
     * is equal to its index
     * @param v
     * @param label
     */
    void SetVertexLabel(int v, string label);

    /**
     * returns the label for vertex v
     * @param v
     * @return 
     */
    string GetVertexLabel(int v);

    /**
     * the structure of the graph, so that it is possible to understand their 
     * vertices and how they relate from their edges.
     */
    void PrintGraph();

    /**
     * auxiliary method that returns the vertex at the u position of the array
     * (vertices)
     * @param u
     * @return 
     */
    Vertex* GetVertex(int u);

};

#endif /* GRAPHAL_H */

