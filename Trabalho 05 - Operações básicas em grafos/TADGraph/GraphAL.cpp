/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GraphAL.cpp
 * Author: vinicius
 * 
 * Created on 6 de Julho de 2018, 20:00
 */

#include "GraphAL.h"
#include "Edge.h"

#include <sstream>

/**
 * 
 * @param n
 */
GraphAL::GraphAL(int n) {
    this->nVertices = n;
    this->nEdges = 0;
    this->Adj = new list<Edge*>[n + 1];
    this->vertices = new Vertex*[n + 1];

    for (int i = 1; i <= n; i++) {
        stringstream ss;
        ss << i;

        Vertex *ver = new Vertex(i, ss.str());
        this->vertices[i] = ver;
    }
}

/**
 * 
 * @return 
 */
int GraphAL::GetNVertices() {
    return this->nVertices;
}

/**
 * 
 * @param u
 * @param v
 * @param dir
 * @param c
 */
void GraphAL::InsertEdge(int u, int v, bool dir, int c) {

    bool flag = false;

    if (!HasEdge(u, v)) {

        if (!dir) {

            if (u == v)
                flag = true;

            Edge *edge2 = new Edge(this->GetVertex(u), dir, c);
            this->Adj[v].push_back(edge2);
        }

        if (flag) // if it is a loop, insert only once
            return;

        Edge *edge1 = new Edge(this->GetVertex(v), dir, c);
        this->Adj[u].push_back(edge1);

        this->nEdges++;
    }
}

/**
 * 
 * @param u
 * @param v
 * @return edge
 */
Edge* GraphAL::HasEdge(int u, int v) {

    for (list<Edge*>::iterator it = this->Adj[u].begin(); it != this->Adj[u].end(); it++) {
        Edge *edge = (*it);

        if ((*it)->GetVertex()->GetKey() == v)
            return edge;
    }
    return NULL;
}

/**
 * 
 * @param u
 * @param v
 * @return 
 */
bool GraphAL::IsDirected(int u, int v) {

    Edge *edge = HasEdge(u, v);

    if (edge)
        return edge->IsDirected();

    return false;
}

/**
 * 
 * @param u
 * @param v
 * @return 
 */
bool GraphAL::IsEdgeValued(int u, int v) {

    Edge *edge = HasEdge(u, v);

    if (edge) {

        if (edge->GetCost() != 0)
            return true;
    }
    return false;
}

/**
 * 
 * @param u
 * @param v
 * @return 
 */
int GraphAL::GetEdgeValue(int u, int v) {

    Edge *edge = this->HasEdge(u, v);

    if (edge)
        return edge->GetCost();

    return 0;
}

/**
 * 
 * @return 
 */
int GraphAL::GetNEdges() {
    return this->nEdges;
}

/**
 * 
 * @param u
 * @param v
 * @return 
 */
Edge* GraphAL::RemoveEdge(int u, int v) {

    Edge *edge = this->HasEdge(u, v);

    if (edge) {

        this->Adj[u].erase(std::remove(this->Adj[u].begin(), this->Adj[u].end(), edge));

        if (!edge->IsDirected()) {

            edge = this->HasEdge(v, u);
            this->Adj[v].erase(std::remove(this->Adj[v].begin(), this->Adj[v].end(), edge));
        }

        this->nEdges--;
        return edge;
    }

    return NULL;
}

/**
 * 
 * @param v
 * @return 
 */
list<Vertex*> GraphAL::GetAdjacencyVertex(int v) {

    list<Vertex*> vertices;

    if (!this->GetVertexLabel(v).empty()) {

        for (list<Edge*>::iterator it = this->Adj[v].begin(); it != this->Adj[v].end(); ++it) {
            Edge *edge = (*it);
            vertices.push_back(edge->GetVertex());
        }
    }
    return vertices;
}

/**
 * 
 * @param v
 * @return 
 */
list<Edge*> GraphAL::GetIncidentEdges(int v) {

    list<Edge*> edges;

    if (!this->GetVertexLabel(v).empty()) {

        for (list<Edge*>::iterator it = this->Adj[v].begin(); it != this->Adj[v].end(); it++) {
            Edge *edge = (*it);
            edges.push_back(edge);
        }
    }
    return edges;
}

/**
 * 
 * @param v
 * @param label
 */
void GraphAL::SetVertexLabel(int v, string label) {

    for (int i = 1; i <= this->nVertices; i++) {

        if (this->vertices[i]->GetKey() == v)
            return this->vertices[i]->SetLabel(label);
    }
}

/**
 * 
 * @param v
 * @return 
 */
string GraphAL::GetVertexLabel(int v) {

    string s;
    for (int i = 1; i <= this->nVertices; i++) {

        if (this->vertices[i]->GetKey() == v)
            s = this->vertices[i]->GetLabel();
    }
    return s;
}

/**
 * 
 */
void GraphAL::PrintGraph() {

    cout << "\n";
    for (int i = 1; i <= this->nVertices; i++) {

        cout << "\n [" << this->vertices[i]->GetLabel() << "]-> ";

        for (list<Edge*>::iterator it = this->Adj[i].begin(); it != this->Adj[i].end(); it++) {
            Edge *edge = (*it);

            if (this->Adj[i].back() == edge)
                cout << edge->GetVertex()->GetLabel();
            else
                cout << edge->GetVertex()->GetLabel() << " -> ";
        }
    }
}

/**
 * 
 * @param u
 * @return 
 */
Vertex* GraphAL::GetVertex(int u) {
    return this->vertices[u];
}

