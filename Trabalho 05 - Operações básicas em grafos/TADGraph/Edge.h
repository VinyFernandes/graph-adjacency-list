/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Edge.h
 * Author: vinicius
 *
 * Created on 6 de Julho de 2018, 20:02
 */

#ifndef EDGE_H
#define EDGE_H

#include "Vertex.h"

class Edge {
private:

    int cost; // Edge cost
    bool directed; // Edge directed
    Vertex *vertex; // adjacent vertex of the vertex array 

public:

    /**
     * default constructor
     */
    Edge();

    /**
     * default constructor with the parameters to set
     * @param vertex
     * @param dir
     * @param cost
     */
    Edge(Vertex *vertex, bool dir, int cost);

    /**
     * default destructor
     */
    virtual ~Edge();

    /**
     * set the edge vertex
     * @param vertex
     */
    void SetVertex(Vertex* vertex);

    /**
     * return the edge vertex
     * @return 
     */
    Vertex* GetVertex() const;

    /**
     * set the edge directed
     * @param directed
     */
    void SetDirected(bool directed);

    /**
     * return the edge directed
     * @return 
     */
    bool IsDirected() const;

    /**
     * set the edge cost
     * @param cost
     */
    void SetCost(int cost);

    /**
     * return the edge cost
     * @return 
     */
    int GetCost() const;

};

#endif /* EDGE_H */

